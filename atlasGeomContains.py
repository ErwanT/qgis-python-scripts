from qgis.core import *
from qgis.gui import *
from qgis.utils import iface

@qgsfunction(args="auto", group='Atlas')
def atlasGeomContains(atlasGeometry, feature, parent):
    """Checks if the feature intersects the atlas geometry"""
    
    return atlasGeometry.contains(feature.geometry())

