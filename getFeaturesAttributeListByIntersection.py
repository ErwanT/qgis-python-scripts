from qgis.core import *
from qgis.gui import *
from qgis.utils import iface

allfeatures = None
index = QgsSpatialIndex()
indexMade = 0
refLayer = None
attrList = None

@qgsfunction(args="auto", group='Custom')
def getFeaturesAttributeListByIntersection(layerName, columnName, geom, feature, parent):
    """Returns all values of a column of all features intersecting a geometry"""
	
    if geom is None:
        raise Exception("No geometry provided")
	
    # globals so we don't create the index, refLayer more than once
    global allfeatures
    global index
    global indexMade
    global refLayer
    global attrList
    attrList = []
	
    # Get the reference layer
    if refLayer is None:
        for layer in iface.legendInterface().layers():
            if layerName == layer.name():
                refLayer = layer
                break
    if refLayer is None:
        raise Exception("Layer [" + layerName + "] not found")
	
    # Create the index if not exists
    if indexMade == 0:
        index = QgsSpatialIndex()
        allAttrs = layer.pendingAllAttributesList()
        layer.select(allAttrs)
        allfeatures = {feature.id(): feature for (feature) in refLayer.getFeatures()}
        for f in allfeatures.values():
            index.insertFeature(f)
        indexMade = 1
	
    # Use spatial index to find intersect 
    fid = None
    ids = index.intersects(geom.boundingBox())
    for id in ids:
        if geom.contains(allfeatures[id].geometry()) and allfeatures[id].attribute(columnName) not in attrList:
            attrList.append(allfeatures[id].attribute(columnName))

    return sorted(attrList)











