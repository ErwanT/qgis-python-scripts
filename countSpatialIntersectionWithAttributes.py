from qgis.core import *
from qgis.gui import *
from qgis.utils import iface

allfeatures = None
indexSpatialIntersectionAttr = QgsSpatialIndex()
indexMadeSpatialIntersectionAttr = 0
refLayer = None

@qgsfunction(args="auto", group='Spatial Functions')
def countSpatialIntersectionWithAttributes(layerName, refColumn, expectedValue, geom, feature, parent):
    """Counts number of feature intersecting geom and having refColumn value equal to expectedValue
	
        Returns the reference column value of the reference layer
        when feature in reference layer intersects the geom
    
        layerName string name of the layer for which we would like to count features number
        refColumn string name of the column in the layer in which we should filter on the expectedValue
        expectedValue string value on which we should filter the features
        geom QgsGeometry geom to use in the intersection
    """

    countFeature = 0

    if geom is None:
		return countFeature

    # globals so we don't create the index, refLayer more than once
    global allfeatures
    global index
    global indexMade
    global refLayer

    # Get the reference layer
    if refLayer is None:
        for layer in iface.legendInterface().layers():
            if layerName == layer.name():
                refLayer = layer
                break
    if refLayer is None:
        raise Exception("Layer [" + layerName + "] not found")

    # Create the index if not exists
    if indexMadeSpatialIntersectionAttr == 0:
        indexSpatialIntersectionAttr = QgsSpatialIndex()
        allAttrs = layer.pendingAllAttributesList()
        refLayer.select(allAttrs)
        allfeatures = {feature.id(): feature for (feature) in refLayer.getFeatures()}
        for f in allfeatures.values():
            indexSpatialIntersectionAttr.insertFeature(f)
        indexMadeSpatialIntersectionAttr = 1

    # Use spatial index to find intersect 
    fid = None
    ids = indexSpatialIntersectionAttr.intersects(geom.boundingBox())
    for id in ids:
		if allfeatures[id].attribute(refColumn) == feature.attribute(expectedValue):
			countFeature = countFeature +1

    # Default
    return countFeature











