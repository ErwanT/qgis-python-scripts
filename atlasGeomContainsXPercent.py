from qgis.core import *
from qgis.gui import *
from qgis.utils import iface

atlasGeometry = None
percentage = None
geom = None
area = None
intersection = None
intersectionArea = None

@qgsfunction(args="auto", group='Atlas')
def atlasGeomContainsXPercent(atlasGeometry, percentage, feature, parent):
    """Checks if x percent of the feature intersects the atlas geometry"""
    
    geom = feature.geometry()
    area = geom.area()
    
    if atlasGeometry.intersects(geom) == False:
        return False
        
    intersection = atlasGeometry.intersection(geom)
    intersectionArea = intersection.area()
    
    return (intersectionArea / area * 100) >= percentage









