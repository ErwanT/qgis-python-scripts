from qgis.core import *
from qgis.gui import *
from qgis.utils import iface

@qgsfunction(args="auto", group='Custom')
def si(condition, onTrue, onFalse, feature, parent):
    """Checks if the feature intersects the atlas geometry"""
    
    if condition:
        return onTrue
    else:
        return onFalse


