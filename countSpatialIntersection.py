from qgis.core import *
from qgis.gui import *
from qgis.utils import iface

allfeatures = None
indexSpatialIntersection = QgsSpatialIndex()
indexCountSpatialIntersection = 0
refLayer = None

@qgsfunction(args="auto", group='Spatial Functions')
def countSpatialIntersection(layerName, geom, feature, parent):
    """Counts number of feature intersecting geom"""

    countFeature = 0

    if geom is None:
		return countFeature

    # globals so we don't create the index, refLayer more than once
    global allfeatures
    global indexSpatialIntersection
    global indexCountSpatialIntersection
    global refLayer

    # Get the reference layer
    if refLayer is None:
        for layer in iface.legendInterface().layers():
            if layerName == layer.name():
                refLayer = layer
                break
    if refLayer is None:
        raise Exception("Layer [" + layerName + "] not found")

    # Create the index if not exists
    if indexCountSpatialIntersection == 0:
        indexSpatialIntersection = QgsSpatialIndex()
        allAttrs = layer.pendingAllAttributesList()
        refLayer.select(allAttrs)
        allfeatures = {feature.id(): feature for (feature) in refLayer.getFeatures()}
        for f in allfeatures.values():
            indexSpatialIntersection.insertFeature(f)
        indexCountSpatialIntersection = 1

    # Use spatial index to find intersect 
    fid = None
    ids = indexSpatialIntersection.intersects(geom.boundingBox())
    for id in ids:
        if geom.contains(allfeatures[id].geometry()):
            countFeature = countFeature + 1

    # Default
    return countFeature





