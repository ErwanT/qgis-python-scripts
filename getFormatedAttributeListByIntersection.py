from qgis.core import *
from qgis.gui import *
from qgis.utils import iface
from string import Template

allfeatures = None
index = QgsSpatialIndex()
indexMade = 0
refLayer = None
attrList = []

@qgsfunction(args="auto", group='Custom')
def getFormatedAttributeListByIntersection(layerName, columnName, templateString, geom, feature, parent):
    """Returns all values of a column of all features intersecting a geometry"""
	
    if geom is None:
        raise Exception("No geometry provided")
	
    # globals so we don't create the index, refLayer more than once
    global allfeatures
    global index
    global indexMade
    global refLayer
    global attrList
    output = ''
    template = Template(templateString)
    
    # Get the reference layer
    if refLayer is None:
        for layer in iface.mapCanvas().layers():
            if layerName == layer.name():
                refLayer = layer
                break
    if refLayer is None:
        raise Exception("Layer [" + layerName + "] not found")
	
    # Create the index if not exists
    if indexMade == 0:
        index = QgsSpatialIndex()
        allAttrs = layer.pendingAllAttributesList()
        layer.select(allAttrs)
        allfeatures = {feature.id(): feature for (feature) in refLayer.getFeatures()}
        for f in allfeatures.values():
            index.insertFeature(f)
        indexMade = 1
	
    # Use spatial index to find intersect 
    ids = index.intersects(geom.boundingBox())
    for id in ids:
        if geom.contains(allfeatures[id].geometry()):
            attrList.append(allfeatures[id].attribute(columnName))
     
    attrList = attrList.sort()
    for el in attrList:
        output = output + template.substitute(element=el)
    
    return output



