# A list of python script to extend QGis expressions

## Description
[QGis](http://www.qgis.org/) is a GIS software that allows to create, edit, visualize, analyze and publish geographic information. Users can write their own functions to use in the expression box of the software.
This projects contains a set of custom expressions to extend QGis.

## License

This code is licensed under [CC0](http://creativecommons.org/publicdomain/zero/1.0/ "Creative Commons Zero - Public Domain").

## Installation
All you have to do is copy the .py files in the .qgis2/python/expressions directory of your home folder.

## Functions in this repository


### applyTemplateForList

This function allows you to format a python list using a template

* pyList list a python list on which the template should be applied
* template string the template to apply on the list. The identifier used is $element, so the template should look like <markup>$element</markup>

#### Usage
```python
applyTemplateForList(["Foo", "Bar", "Baz"], "<li>$element</li>")
```

```html
<li>Foo</li>
<li>Bar</li>
<li>Baz</li>
```

### atlasFilterByLayer

This functions allows you to set a filter for an atlas coverage layer, that will return only atlas features that contains at least a feature of a given layer

* layerName string the layer that sould be used to filter the atlas coverage layer

#### Usage
```python
atlasFilterByLayer("myLayer")
```

### atlasGeomContains

This functions allows you to check if an atlas geometry contains the current feature

* atlasGeom geometry the geomety that should contains the feature

#### Usage
```python
atlasGeomContains($atlasgeometry)
```

### atlasGeomContainsXPercent

This functions allows you to check if an atlas geometry contains at least x percent of the current feature

* atlasGeom geometry the geomety that should contains the feature
* percentage float tthe minimum percentage that the geometry should contains

#### Usage
```python
atlasGeomContainsXPercent($atlasgeometry, 80)
```

### countSpatialIntersection

This functions counts the number of features of a given layer intersecting a geom

* layerName string the layer in which you would like to count the features
* geom geometry the geometry that should be intersecting the features

#### Usage
```python
countSpatialIntersection('myLayer', $atlasgeometry)
```

### countSpatialIntersectionWithAttributes

This functions counts the number of features of a given layer that has a specific attribute value and intersects a geom

* layerName string the layer in which you would like to count the features
* refColumn string the attribute column
* expectedValue string the value that should have the attribute
* geom geometry the geometry that should be intersecting the features

#### Usage
```python
countSpatialIntersectionWithAttributes('myLayer', 'myColumn', 'Bar', $atlasgeometry)
```

### getFeaturesAttributeListByIntersection

This functions returns a python list of all deduplicate attributes value of the features of a given layer that intersects a geometry

* layerName string the layer in which you would like to count the features
* columnName string the attribute column
* geom geometry the geometry that should be intersecting the features

#### Usage
```python
getFeaturesAttributeListByIntersection('myLayer', 'myColumn', $atlasgeometry)
```

### getFormatedAttributeListByIntersection

This functions returns all deduplicate attributes value of the features of a given layer that intersects a geometry formatted by a template

* layerName string the layer in which you would like to count the features
* columnName string the attribute column
* geom geometry the geometry that should be intersecting the features

#### Usage
```python
getFeaturesAttributeListByIntersection('myLayer', 'myColumn', '<li>$element</li>', $atlasgeometry)
```

### si

This functions test a condition and returns the given true and false value

* condition string the condition to test
* onTrue mixed the value to return when the result is true
* onFalse mixed the value to return when the result is false

#### Usage
```python
si(condition, onTrue, onFalse)
```

### spatialJoinLookup

This functions returns the attribute of the feature in the provided layer that intersects the geometry

* layerName string the layer in which you would like to count the features
* refColumn string the attribute column
* defaultValue string the value to return when there isn't any features in layerName that intersects the geometry
* geom geometry the geometry that should be intersecting the features

#### Usage
```python
spatialJoinLookup('myLayer', 'myColumn', 'Foo', $geometry)
```

