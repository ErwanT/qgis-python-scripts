from qgis.core import *
from qgis.gui import *
from qgis.utils import iface

allfeatures = None
indexFilterByLayer = {}
indexMadeFilterByLayer = 0
refLayer = None

@qgsfunction(args="auto", group='Atlas')
def atlasFilterByLayer(layerName, feature, parent):
    """Checks if the features of the layer intersects the atlas geometry
    
        layerName string name of the layer which features should intersects atlas geometry
    """
    
    global allfeatures
    global indexFilterByLayer
    global indexMadeFilterByLayer
    global refLayer
    
    atlasGeom = feature.geometry()
    layer = None
    
    # Get the reference layer
    if refLayer is None:
        for layer in iface.legendInterface().layers():
            if layerName == layer.name():
                refLayer = layer
                break
    if refLayer is None:
        raise Exception("Layer [" + layerName + "] not found")

    # Create the index if not exists
    if indexFilterByLayer.has_key(layerName) == False:
        localIdx = QgsSpatialIndex()
        allAttrs = refLayer.pendingAllAttributesList()
        refLayer.select(allAttrs)
        allfeatures = {feature.id(): feature for (feature) in refLayer.getFeatures()}
        for f in allfeatures.values():
            localIdx.insertFeature(f)
        indexFilterByLayer[layerName] = localIdx
    else:
        localIdx = indexFilterByLayer[layerName]
    
    # Use spatial index to find intersect 
    fid = None
    ids = localIdx.intersects(atlasGeom.boundingBox())
    for id in ids:
        if atlasGeom.contains(allfeatures[id].geometry()):
            fid = id
            break # Only get the first match.
    if fid is not None:
        return True
    
    return False








