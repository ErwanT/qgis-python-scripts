from qgis.core import *
from qgis.gui import *
from qgis.utils import iface
from string import Template


@qgsfunction(args="auto", group='Custom')
def applyTemplateForList(pyList, templateString, feature, parent):
    """Apply a string template for a list. Template should use the $element keyword to identify list element"""
    
    output = ''
    template = Template(templateString)
     
    for el in pyList:
        output = output + template.substitute(element=el)
    
    return output








